import { max } from "d3-array";
import { scaleQuantize } from "d3-scale";
import { select } from "d3-selection";
import moment from "moment";
//import { CalendarYearLayout as CYL, ChartLabel, LinearGrid } from "@lgv/visualization-chart";
import { ChartLabel, LinearGrid } from "@lgv/visualization-chart";
import { CalendarYearLayout as CYL } from "@lgv/visualization-chart";

import { configuration, configurationLayout } from "../configuration.js";
moment.updateLocale('en', {
    week: {
      dow : 0, // Monday is the first day of the week.
      doy: 6
    }
  });
  
/**
 * CalendarYear is a time series visualization.
 * @param {array} data - objects where each represents a path in the hierarchy
 * @param {integer} height - artboard height
 * @param {float} radiusMax - max of radii
 * @param {float} radiusMin - min of radii
 * @param {integer} width - artboard width
 */
class CalendarYear extends LinearGrid {
    constructor(data, width=configurationLayout.width, height=configurationLayout.height, CalendarLayout=null, color=null, label=configuration.branding, name=configuration.name) {
        
        // initialize inheritance
        super(data, width, height, CalendarLayout ? CalendarLayout : new CYL(data, {height:height,width:width}), label, name);

        // update self
        this.cell = null;
        this.classCell = `${label}-cell`;
        this.classDay = `${label}-day`;
        this.classLabelMonth = `${label}-label-month`;
        this.classLabelWeekday = `${label}-label-weekday`;
        this.classLabelYear = `${label}-label-year`;
        this.classMonth = `${label}-month`;
        this.classWeekday = `${label}-weekday`;
        this.classYear = `${label}-year`;
        this.color = color || ["transparent", "lightgrey", "grey"];
        this.day = null;
        this.labelMonth = null;
        this.labelWeekday = null;
        this.labelYear = null;
        this.month = null;
        this.weekday = null;
        this.year = null;
        console.log(this)
    }

    /**
     * Construct value scale to organize values into discrete thresholds.
     * @returns A d3 scale function.
     */
    get valueScale() {
        let values = Array.from(this.Data.aggregate).map(d => ({ date: d[0], count: d[1] }));
        return scaleQuantize()
            .domain([0, max(values.map(d => this.Data.extractValue(d)))])
            .range(this.color);
    }

    /**
     * Configure calendar cell DOM events.
     */
    configureCellEvents() {
        this.cell
            .on("click", (e,d) => this.configureEvent("cell-click",d,e))
            .on("mouseover", (e,d) => {

                // update class
                select(e.target).attr("class", `${this.classCell} active`);

                // send event to parent
                this.configureEvent("cell-mouseover",d,e);

            })
            .on("mouseout", (e,d) => {

                // update class
                select(e.target).attr("class", this.classCell);

                // send event to parent
                this.artboard.dispatch("cell-mouseout", {
                    bubbles: true
                });

            });
    }

    /**
     * Position and minimally style cell in SVG dom element.
     */
    configureCells() {
        this.cell
            .attr("class", this.classCell)
            .attr("data-value", d => this.Data.extractValue(d))
            .attr("data-label", d => d)
            .attr("x", d => {
                let values = d.split("-");
                let year = values[0];
                let monthDay = `${values[1]}-${values[2]}`;
                let week = monthDay == "12-31" && moment(d).week() == 1 ? moment(`${year}-12-28`).week() + 1 : moment(d).week();
                return (week - 1) * this.Data.cellWidth;
            })
            .attr("y", d => (moment(d).weekday() - 1) * this.Data.cellHeight)
            .attr("height", this.Data.cellHeight)
            .attr("width", this.Data.cellWidth)
            .attr("stroke", "black")
            .attr("fill", d => {
                let count = this.Data.aggregate.get(d);
                let result = count ? count : 0;
                return this.valueScale(result);
            })
    }

    /**
     * Position and minimally style day group in SVG dom element.
     */
    configureDays() {
        this.day
            .attr("class", this.classDay)
            .attr("transform", `translate(${this.unit},${this.Label.determineHeight(this.Data.months.join())*2})`);
    }

    /**
     * Position and minimally style month labels in SVG dom element.
     */
    configureMonthLabels() {
        this.labelMonth
            .attr("class", this.classLabelMonth)
            .attr("data-label", d => d)
            .attr("pointer-events", "none")
            .attr("x", (d,i) => (moment(`${d.year}-${i<9 ? 0 : ""}${i+1}-01`).week() - 1) * this.Data.cellWidth)
            .attr("y", this.Data.cellHeight)
            .text(d => d.month[0]);
    }

    /**
     * Position and minimally style month group in SVG dom element.
     */
    configureMonths() {
        this.month
            .attr("class", this.classMonth)
            .attr("transform", (d,i) => `translate(${this.unit},${(i * this.Data.yearHeight) + this.unit})`);
    }

    /**
     * Position and minimally style weekday group in SVG dom element.
     */
    configureWeekdays() {
        this.weekday
            .attr("class", this.classWeekday)
            .attr("transform", `translate(0,${this.Label.determineHeight(this.Data.months.join())*2})`);
    }

    /**
     * Position and minimally style year group in SVG dom element.
     */
    configureYears() {
        this.year
            .attr("class", this.classYear)
            .attr("transform", (d,i) => `translate(0,${(i * this.Data.yearHeight) + (i * (this.Data.yearPadding))})`);
    }

    /**
     * Position and minimally style weekday labels in SVG dom element.
     */
    configureWeekdayLabels() {
        this.labelWeekday
            .attr("class", this.classLabelWeekday)
            .attr("data-label", d => d)
            .attr("pointer-events", "none")
            .attr("x", 0)
            .attr("y", (d,i) => i * this.Data.cellHeight)
            .text(d => d[0]);
    }

    /**
     * Position and minimally style year labels in SVG dom element.
     */
    configureYearLabels() {
        this.labelYear
            .attr("class", this.classLabelYear)
            .attr("data-label", d => d)
            .attr("pointer-events", "none")
            .attr("x", 0)
            .attr("y", d => this.Label.determineHeight(d) * 0.65)
            .text(d => d);
    }


    /**
     * Generate visualization.
     */
    generateChart() {

        // initialize labeling
        this.Label = new ChartLabel(this.artboard, this.Data, this.unit);

        // update data based on label sizing
        /*this.Data.cellHeight = (this.Data.yearHeight - (this.Label.determineHeight(this.Data.weekdays.join())*1.65)) / this.Data.weekdays.length;
        //this.Data.cellWidth = (this.params.width - this.unit) / 54;

        // generate year group
        this.year = this.generateYears(this.content);
        this.configureYears();

        // generate year labels
        this.labelYear = this.generateYearLabels(this.year);
        this.configureYearLabels();

        // generate weekday labels
        this.weekday = this.generateWeekdays(this.year);
        this.configureWeekdays();
        this.labelWeekday = this.generateWeekdayLabels(this.weekday);
        this.configureWeekdayLabels();

        // generate month labels
        this.month = this.generateMonths(this.year);
        this.configureMonths();
        this.labelMonth = this.generateMonthLabels(this.month);
        this.configureMonthLabels();

        // generate cells
        this.day = this.generateDays(this.year);
        this.configureDays();
        this.cell = this.generateCells(this.day);
        this.configureCells();
        this.configureCellEvents();*/

    }

    /**
     * Generate cell rectangle in SVG element.
     * @param {node} domNode - d3.js SVG selection
     */
    generateCells(domNode) {
        return domNode
            .selectAll(`.${this.classCell}`)
            .data(d => this.Data.generateDates(`${d[0]}-01-01`,`${d[0]}-12-31`))
            .join(
                enter => enter.append("rect"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate day group in SVG element.
     * @param {node} domNode - d3.js SVG selection
     */
    generateDays(domNode) {
        return domNode
            .selectAll(`.${this.classDay}`)
            .data(d => [d])
            .join(
                enter => enter.append("g"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate month labels in SVG element.
     * @param {node} domNode - d3.js SVG selection
     */
    generateMonthLabels(domNode) {
        return domNode
            .selectAll(`.${this.classLabelMonth}`)
            .data(d => this.Data.months.map(x => ({ year: d, month: x})))
            .join(
                enter => enter.append("text"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate month group in SVG element.
     * @param {node} domNode - d3.js SVG selection
     */
    generateMonths(domNode) {
        return domNode
            .selectAll(`.${this.classMonth}`)
            .data(d => [d[0]])
            .join(
                enter => enter.append("g"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate weekday group in SVG element.
     * @param {node} domNode - d3.js SVG selection
     */
    generateWeekdays(domNode) {
        return domNode
            .selectAll(`.${this.classWeekday}`)
            .data(d => [d[0]])
            .join(
                enter => enter.append("g"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate year group in SVG element.
     * @param {node} domNode - d3.js SVG selection
     */
    generateYears(domNode) {
        return domNode
            .selectAll(`.${this.classYear}`)
            .data(d => this.data)
            .join(
                enter => enter.append("g"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate weekday labels in SVG element.
     * @param {node} domNode - d3.js SVG selection
     */
    generateWeekdayLabels(domNode) {
        return domNode
            .selectAll(`.${this.classLabelWeekday}`)
            .data(d => this.Data.weekdays)
            .join(
                enter => enter.append("text"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate year labels in SVG element.
     * @param {node} domNode - d3.js SVG selection
     */
    generateYearLabels(domNode) {
        return domNode
            .selectAll(`.${this.classLabelYear}`)
            .data(d => [d[0]])
            .join(
                enter => enter.append("text"),
                update => update,
                exit => exit.remove()
            );
    }

};

export { CalendarYear };
export default CalendarYear;
