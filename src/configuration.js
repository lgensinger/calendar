import { configuration as config } from "@lgv/visualization-chart";
import packagejson from "../package.json";

const configuration = {
    branding: process.env.LGV_BRANDING || config.branding,
    name: packagejson.name.replace("/", "-").slice(1)
};

const configurationLayout = {
    height: process.env.LGV_HEIGHT || 600,
    width: process.env.LGV_WIDTH || 600
}

export { configuration, configurationLayout };
export default configuration;
