import { select } from "d3-selection";
import moment from "moment";
import { ChartLabel, DomGrid, CalendarData as CD } from "@lgv/visualization-chart";

import { configuration } from "../configuration.js";
import style from "./style.css";

/**
 * CalendarMonth is a time series visualization.
 * @param {array} data - objects where each represents a path in the hierarchy
 * @param {CalendarData} class - JavaScript class with calendar helpers
 * @param {string} date - iso 8601 date value
 * @param {integer} height - artboard height
 * @param {integer} width - artboard width
 */
class CalendarMonth extends DomGrid {
    constructor(data, date=null, CalendarData=null, label=configuration.branding, name=configuration.name) {
        
        // initialize inheritance
        super(data, CalendarData ? CalendarData : new CD(data), label, name);

        // update self
        this.cell = null;
        this.cellHeader = null;
        this.classCell = `${label}-cell`;
        this.classCellHeader = `${label}-cell-header`;
        this.classDay = `${label}-day`;
        this.classDow = `${label}-dow`;
        this.classEvent = `${label}-event`;
        this.classEventGrid = `${label}-event-grid`;
        this.classMore = `${label}-more`;
        this.classStat = `${label}-stat`;
        this.classWeekday = `${label}-weekday`;
        this.contextDate = date ? date : moment().format("YYYY-MM-DD");
        this.current = null;
        this.day = null;
        this.dow = null;
        this.event = null;
        this.eventGrid = null;
        this.monthReference = {};
        this.more = null;
        this.next = null;
        this.previous = null;
        this.stat = null;
        this.weekday = null;

        // construct empty calendar grid
        this.gridDates = this.constructCalendarGrid(date);

    }

    /**
     * Extract cell height.
     */
    get cellHeight() {
        return this.cell.node().offsetHeight;
    }

    /**
     * Extract cell width.
     */
    get cellWidth() {
        return this.cell.node().offsetWidth;
    }

    /**
     * Given the cell height how many events can fit the available height.
     */
    get eventGridCount() {
        return parseInt(this.eventGridHeight / this.eventHeight);
    }

    /**
     * Determine available space for events within cell below date notation.
     */
    get eventGridHeight() {
        let cellHeader = this.cellHeader.node().offsetHeight;
        let moreSpace = this.eventHeight * 2;
        return this.cellHeight - cellHeader - moreSpace;
    }

    /**
     * Determine spacing between events without adding default extra for first/last items in stack.
     */
    get eventGridSpacer() {
        return this.unit * 0.1;
    }

    /**
     * Determine event height based on available space.
     */
    get eventHeight() {
        return this.unit;
    }

    /**
     * Prune grid dates to business week.
     */
    get prunedGridDates() {
        return this.gridDates;
    }

    /**
     * Establish month change event details.
     */
    get monthChange() {
        return {
            bubbles: true,
            detail: {
                current: {
                    date: this.current.format("YYYY-MM-DD"),
                    name: this.current.format("MMMM"),
                    year: this.current.year()
                },
                next: {
                    date: this.next.format("YYYY-MM-DD"),
                    name: this.next.format("MMMM"),
                    year: this.next.year()
                },
                previous: {
                    date: this.previous.format("YYYY-MM-DD"),
                    name: this.previous.format("MMMM"),
                    year: this.previous.year()
                }
            }
        };
    }

    /**
     * Configure a JavaScript day event.
     * @param {d} object - d3.js data object
     * @param {e} Event - JavaScript Event object
     * @param {id} string - event id
     */
    configureDayEvent(id, d, e, z) {
        this.artboard.dispatch(id, {
            bubbles: true,
            detail: this.Data.configureDayEventDetails(d,e,z)
        });
    }

    /**
     * Declare event bar mouse events.
     */
    configureEventEvents() {
        this.event
            .on("click", (e,d) => this.configureEvent("event-click",d,e))
            .on("mouseover", (e,d) => {

                // update class
                select(e.target).attr("class", `${this.classEvent} active`);

                // send event to parent
                this.configureEvent("event-mouseover",d,e);

            })
            .on("mouseout", (e,d) => {

                // update class
                select(e.target).attr("class", this.classEvent);

                // send event to parent
                this.artboard.dispatch("event-mouseout", {
                    bubbles: true
                });

            });
    }

    /**
     * Declare day digit mouse events.
     */
    configureDayEvents() {
        this.day
            .on("click", (e,d) => this.configureDayEvent("day-click",d,e))
            .on("mouseover", (e,d) => {

                // update class
                select(e.target).attr("class", `${this.classDay} active`);

                // send event to parent
                this.configureDayEvent("day-mouseover",d,e);

            })
            .on("mouseout", (e,d) => {

                // update class
                select(e.target).attr("class", this.classDay);

                // send event to parent
                this.artboard.dispatch("day-mouseout", {
                    bubbles: true
                });

            });
    }

    /**
     * Declare more events mouse events.
     */
    configureMoreEvents() {
        this.more
            .on("click", (e,d) => this.configureDayEvent("more-click",d,e))
            .on("mouseover", (e,d) => {

                // update class
                select(e.target).attr("class", `${this.classMore} active`);

                // send event to parent
                this.configureDayEvent("more-mouseover",d,e);

            })
            .on("mouseout", (e,d) => {

                // update class
                select(e.target).attr("class", this.classMore);

                // send event to parent
                this.artboard.dispatch("more-mouseout", {
                    bubbles: true
                });

            });
    }

    /**
     * Position and minimally style cell header in dom element.
     */
    configureCellHeaders() {
        this.cellHeader
            .attr("class", this.classCellHeader);
    }

    /**
     * Position and minimally style cell in dom element.
     */
    configureCells() {
        this.cell
            .attr("class", this.classCell)
            .attr("data-is-today", d => moment(d).format("YYYY-MM-DD") === moment().format("YYYY-MM-DD") ? "true" : null)
            .attr("data-label", d => d)
            .attr("data-month", d => Object.keys(this.monthReference).includes(moment(d).format("YYYY-MM")) ? this.monthReference[moment(d).format("YYYY-MM")] : moment(this.contextDate).diff(d) > 0 ? "previous" : (moment(this.contextDate).diff(d) < 0 ? "next" : "current"))
            .style("height", d => `${this.cell.node().offsetWidth*0.9}px`)
            .style("pointer-events", "none");
    }

    /**
     * Position and minimally style containing group in dom element.
     */
    configureContainer() {
        this.content
            .attr("class", this.classContainer);
            
    }

    /**
     * Position and minimally style day date in dom element.
     */
    configureDays() {
        this.day
            .attr("class", this.classDay)
            .style("pointer-events", "all")
            .html(d => d.split("-")[2]);
    }

    /**
     * Position and minimally style days of week group in dom element.
     */
    configureDow() {
        this.dow
            .attr("class", this.classDow);
    }

    /**
     * Position and minimally style events container in dom element.
     */
    configureEventGrids() {
        this.eventGrid
            .attr("class", this.classEventGrid)
            .style("margin-top", d => d.length > 0 && d[0].renderOffset > 0 ? `${(d[0].renderOffset * this.eventHeight) + (this.eventGridSpacer * d[0].renderOffset)}px` : null);
    }

    /**
     * Position and minimally style event in dom element.
     */
    configureEvents() {
        this.event
            .attr("class", this.classEvent)
            .attr("data-value", d => this.Data.extractValue(d))
            .attr("data-label", d => this.Data.extractLabel(d))
            .style("height", this.eventHeight)
            .style("width", d => `calc((100% * ${d.renderLength+1}) + (var(--gutter) * ${d.renderLength == 0 ? 0 : d.renderLength}))`)
            .style("margin-top", (d,i) => i == 0 ? null : `${this.eventGridSpacer}px`)
            .style("pointer-events", "all")
            .html(d => d.event_title);
    }

    /**
     * Position and minimally style more events in dom element.
     */
    configureMore() {
        this.more
            .attr("class", this.classMore)
            .style("pointer-events", "all")
            .html(d => `<span style="font-size:1.25em">⬒</span> ${d[0]} more`);
    }

    /**
     * Position and minimally style stats in dom element.
     */
    configureStats() {
        this.stat
            .attr("class", this.classStat)
            .html(d => d);
    }

    /**
     * Position and minimally style weekday in dom element.
     */
    configureWeekday() {
        this.weekday
            .attr("class", this.classWeekday)
            .html(d => d[0]);
    }

    /**
     * Generate calendar grid of month.
     * @param {string} date - iso-8601 date value
     */
    constructCalendarGrid(date) {

        // generate previous/next month values for navigation
        this.determineMonths(date);

        // generate date values for current month
        return this.generateCalendarGridValues();

    }

    /**
     * Determine previous/next month values for navigation
     * @param {string} date - iso-8601 date value
     */
    determineMonths(date) {

        // set month values to expose in events
        this.current = moment(this.contextDate);
        this.next = moment(this.current.format("YYYY-MM-DD")).add(1,"month");
        this.previous = moment(this.current.format("YYYY-MM-DD")).subtract(1,"month");
        this.monthReference[this.current.format("YYYY-MM")] = "current";
        this.monthReference[this.next.format("YYYY-MM")] = "next";
        this.monthReference[this.previous.format("YYYY-MM")] = "previous";

    }

    /**
     * Generate calendar grid iso 8601 date values.
     */
    generateCalendarGridValues() {

        // set grid start/end
        let currentStart = moment(this.current).startOf("month");
        let weekStart = moment(currentStart).startOf("isoweek");
        let currentEnd = moment(currentStart).endOf("month");
        let weekEnd = moment(currentEnd).endOf("isoweek");
        let weekCount = moment(weekEnd).diff(moment(weekStart),"week");

        // generate grid
        return [...Array(weekCount+1).keys()].map(d => {
            let wm = moment(weekStart).add(d,"week");
            return this.Data.generateDates(
                wm.startOf("isoweek").format("YYYY-MM-DD"),
                wm.endOf("isoweek").format("YYYY-MM-DD")
            );
        }).flat();

    }

    /**
     * Generate cell header shape in DOM element.
     * @param {selection} selection - d3.js selection
     */
    generateCellHeaders(selection) {
        return selection
            .selectAll(`.${this.classCellHeader}`)
            .data(d => [d])
            .join(
                enter => enter.append("hgroup"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate cell rectangle in DOM element.
     * @param {selection} selection - d3.js selection
     */
    generateCells(selection) {
        return selection
            .selectAll(`.${this.classCell}`)
            .data(d => this.prunedGridDates)
            .join(
                enter => enter.append("div"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate visualization.
     */
    generateChart() {
        
        // broadcast updates
        this.artboard.node().dispatchEvent(new CustomEvent("month-change", this.monthChange));

        // initialize labeling
        this.Label = new ChartLabel(this.artboard, this.Data);

        // generate each cell
        this.cell = this.generateCells(this.content);
        this.configureCells();

        // generate cell header
        this.cellHeader = this.generateCellHeaders(this.cell);
        this.configureCellHeaders();

        // generate each cell date
        this.day = this.generateDays(this.cellHeader);
        this.configureDays();
        this.configureDayEvents();

        // generate each cell stat
        this.stat = this.generateStats(this.cellHeader);
        this.configureStats();

        // generate event grid
        this.eventGrid = this.generateEventGrids(this.cell);
        this.configureEventGrids();

        // generate each event
        this.event = this.generateEvents(this.eventGrid);
        this.configureEvents();
        this.configureEventEvents();

        // generate more events
        this.more = this.generateMore(this.cell);
        this.configureMore();
        this.configureMoreEvents();
        
    }

    /**
     * Generate artboard and container for visualization.
     */
    generateCore() {

        // generate svg artboard
        this.artboard = this.generateArtboard(this.container);
        this.configureArtboard();

        // generate group for dow
        this.dow = this.generateDow(this.artboard);
        this.configureDow();

        // generate days of week
        this.weekday = this.generateWeekday(this.dow);
        this.configureWeekday();

        // wrap for content to ensure render within artboard
        this.content = this.generateContainer(this.artboard);
        this.configureContainer();

    }

    /**
     * Generate day date shape in DOM element.
     * @param {selection} selection - d3.js selection
     */
    generateDays(selection) {
        return selection
            .selectAll(`.${this.classDay}`)
            .data(d => [d])
            .join(
                enter => enter.append("h1"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate day of week group in DOM element.
     * @param {selection} selection - d3.js selection
     */
    generateDow(selection) {
        return selection
            .selectAll(`.${this.classDow}`)
            .data([this.Data.weekdays])
            .join(
                enter => enter.append("div"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate events container in DOM element.
     * @param {selection} selection - d3.js selection
     */
    generateEventGrids(selection) {
        return selection
            .selectAll(`.${this.classEventGrid}`)
            .data(d => {
                let values = this.data.get(d);
                let sorted = values ? values.sort((a,b) => b.duration - a.duration) : null;
                let renderOffset = sorted ? sorted[0].renderOffset : 0;
                return values ? [sorted.filter(x => x.date == x.renderDate).filter((x,i) => i < (this.eventGridCount - renderOffset))] : [];
            })
            .join(
                enter => enter.append("div"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate event shape in DOM element.
     * @param {selection} selection - d3.js selection
     */
    generateEvents(selection) {
        return selection
            .selectAll(`.${this.classEvent}`)
            .data(d => d)
            .join(
                enter => enter.append("p"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate more events shape in DOM element.
     * @param {selection} selection - d3.js selection
     */
    generateMore(selection) {
        return selection
            .selectAll(`.${this.classMore}`)
            .data(d => this.data.get(d) && this.data.get(d).length > this.eventGridCount  ? [[this.data.get(d).length - (this.eventGridCount < 0 ? 0 : this.eventGridCount), this.data.get(d).filter(x => x.date !== x.renderDate).length]] : [])
            .join(
                enter => enter.append("p"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate stat shape in DOM element.
     * @param {selection} selection - d3.js selection
     */
    generateStats(selection) {
        return selection
            .selectAll(`.${this.classStat}`)
            .data(d => this.data.get(d) ? [this.data.get(d).length] : [])
            .join(
                enter => enter.append("p"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate weekday in DOM element.
     * @param {selection} selection - d3.js selection
     */
    generateWeekday(selection) {
        return selection
            .selectAll(`.${this.classWeekday}`)
            .data(d => d)
            .join(
                enter => enter.append("p"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Update visualization.
     * @param {object} data - key/values where each object represents an event
     * @param {string} date - iso 8601 date value from which to construct a month
     */
    update(data=null,date=null) {

        // check for change of date
        if (date && date !== this.contextDate) {

            // update self
            this.contextDate = date;

            // reset the empty calendar grid
            this.gridDates = this.constructCalendarGrid(date);

        }

        // check for data
        if (data) {

            // recalculate values
            this.Data.source = data;
            this.Data.data = this.Data.update;
            this.data = this.Data.data;
    
        }

        // re-generate visualization
        this.generateVisualization();
    }

};

export { CalendarMonth };
export default CalendarMonth;