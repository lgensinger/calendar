import test from "ava";

import { configuration, configurationLayout } from "../src/configuration.js";
import { Calendar } from "../src/index.js";

/******************** EMPTY VARIABLES ********************/

// initialize
/*let b = new Beeswarm();

// TEST INIT //
test("init", t => {

    t.true(b.height === configurationLayout.height);
    t.true(b.width === configurationLayout.width);

});

// TEST RENDER //
// have to turn off until d3.transition bug fixed
/*test("render", t => {

    // clear document
    document.body.innerHTML = "";

    // render to dom
    bc.render(document.body);

    // get generated element
    let artboard = document.querySelector(`.${configuration.name}`);

    t.true(artboard !== undefined);
    t.true(artboard.nodeName == "svg");
    t.true(artboard.getAttribute("viewBox").split(" ")[3] == configurationLayout.height);
    t.true(artboard.getAttribute("viewBox").split(" ")[2] == configurationLayout.width);

});*/

/******************** DECLARED PARAMS ********************/

let testWidth = 300;
let testHeight = 500;
let testData = [
    {label: "xyz", id: 1, value: 1, x: 4, y: 1},
    {label: "abc", id: 2, value: 4, x: 9, y: 5}
];

// initialize
let bt = new Calendar(testData, testWidth, testHeight);

// TEST INIT //
test("init_params", t => {

    t.true(bt.height === testHeight);
    t.true(bt.width === testWidth);

});

// TEST RENDER //
// have to turn off until d3.transition bug fixed
/*test("render_params", t => {

    // clear document
    document.body.innerHTML = "";

    // render to dom
    bct.render(document.body);

    // get generated element
    let artboard = document.querySelector(`.${configuration.name}`);

    t.true(artboard !== undefined);
    t.true(artboard.nodeName == "svg");
    t.true(artboard.getAttribute("viewBox").split(" ")[3] == testHeight);
    t.true(artboard.getAttribute("viewBox").split(" ")[2] == testWidth);

});*/
