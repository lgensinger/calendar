import moment from "moment";

import packageJson from "../package.json";
import { CalendarMonth } from "../src/index.js";
import { processEvent, renderDefault } from "@lgv/visualization-chart";

let data = [{ date_start: moment().format("YYYY-MM-DD"), date_end: moment().format("YYYY-MM-DD"), event_title: "Event 1"}];

let c = null;

// get elements
let container = document.getElementsByTagName("figure")[0];
let sourceContainer = document.getElementsByTagName("code")[0];
let outputContainer = document.getElementsByTagName("code")[1];
let previous = document.querySelector("#previous");
let next = document.querySelector("#next");
let update = document.querySelector("#update");

// attach events
container.outputContainer = outputContainer;
container.addEventListener("day-click",processEvent);
container.addEventListener("day-mouseover", processEvent);
container.addEventListener("day-mouseout", processEvent);
container.addEventListener("event-click",processEvent);
container.addEventListener("event-mouseover", processEvent);
container.addEventListener("event-mouseout", processEvent);
container.addEventListener("month-change", processMonthChange);
container.addEventListener("more-click",processEvent);
container.addEventListener("more-mouseover", processEvent);
container.addEventListener("more-mouseout", processEvent);
previous.addEventListener("click", processMonthNavigation);
next.addEventListener("click", processMonthNavigation);
update.addEventListener("click", processUpdatedData);

/**
 * Process month change.
 * @param {e} Event - JavaScriipt Event
 */
function processMonthChange(e) {
    
    // add title for visualization
    let vh1 = document.querySelector("#title");
    vh1.innerText = `${e.detail.current.name} ${e.detail.current.year}`;

    // update attributes on buttons
    next.innerText = e.detail.next.name;
    next.setAttribute("data-month",e.detail.next.date);
    previous.innerText = e.detail.previous.name;
    previous.setAttribute("data-month",e.detail.previous.date);

    // also show in event dialog
    processEvent(e);

}

/**
 * Process month change to next month.
 * @param {e} Event - JavaScriipt Event
 */
function processMonthNavigation(e) {

    // get date bound to button
    let monthDate = e.target.dataset.month;
    
    // update viz
    c.update(null,monthDate);

}

/**
 * Process event change.
 * @param {e} Event - JavaScriipt Event
 */
function processUpdatedData(e) {
    
    // update viz
    c.update(data.concat([...new Array(5).keys().map(d => ({
        date_end: moment().add(d+1,"day").format("YYYY-MM-DD"),
        date_start: moment().subtract(d+1,"day").format("YYYY-MM-DD"),
        event_title: `Event ${d+2} plus a lot more to make sure this is a longish title and stuff here and woof bark yo`
    }))]));

}

/**
 * Render initial visualization.
 * @param {element} container - DOM element
 * @param {array} data - object with key/value pairs of path
 */
function startup(data,container) {

    // update version in header
    let h1 = document.querySelector("h1");
    let title = h1.innerText;
    h1.innerHTML = `${title} <span>v${packageJson.version}</span>`;

    // render source data
    renderDefault(data,sourceContainer,outputContainer);

    // determine configs
    let width = container.offsetWidth;
    
    // initialize
    c = new CalendarMonth(data);

    // render visualization
    c.render(container);

}

// load document
document.onload = startup(data,container);