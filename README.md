# Calendar

ES6 d3.js calendar visualization.

## Install

```bash
# install package
npm install @lgv/calendar
```

## Data Format

The following values are the expected input data structure; id can be omitted.

```json
[
    {
        "id": 1,
        "date_start": "2024-01-01",
        "date_end": "2024-01-02",
        "event_title": "Event A"
    },
    {
        "id": 2,
        "date_start": "2024-01-01",
        "date_end": "2024-01-01",
        "event_title": "Event B"
    },
]
```

## Use Module

```bash
import { Calendar } from "@lgv/calendar";

// have some data
let data = [
    {
        "id": 1,
        "date_start": "2024-01-01",
        "date_end": "2024-01-02",
        "event_title": "Event A"
    },
    {
        "id": 2,
        "date_start": "2024-01-01",
        "date_end": "2024-01-01",
        "event_title": "Event B"
    },
];

// initialize
const b = new Calendar(data);

// render visualization
b.render(document.body);
```

## Environment Variables

The following values can be set via environment or passed into the class.

| Name | Type | Description |
| :-- | :-- | :-- |
| `LGV_HEIGHT` | integer | height of artboard |
| `LGV_WIDTH` | integer | width of artboard |

## Events

The following events are dispatched from the svg element. Hover events toggle a class named `active` on the element.

| Target | Name | Event |
| :-- | :-- | :-- |
| date digit | `day-click` | on click |
| date digit | `day-mouseover` | on hover |
| date digit | `day-mousemout` | on un-hover |
| event bar | `event-click` | on click |
| event bar | `event-mouseover` | on hover |
| event bar | `event-mousemout` | on un-hover |
| month name | `month-change` | on click |
| more text | `more-click` | on click |
| more text | `more-mouseover` | on hover |
| more text | `more-mousemout` | on un-hover |

## Style

Style is expected to be addressed via css. Any style not met by the visualization module is expected to be added by the importing component.

| Class | Element |
| :-- | :-- |
| `lgv-cell` | div element |
| `lgv-cell-header` | div element element |
| `lgv-day` | h1 element |
| `lgv-dow` | div element holding all column elements |
| `lgv-event` | div element |
| `lgv-event-grid` | div element holding all event divs for a given cell |
| `lgv-more` | p element indicating more events for a given cell |
| `lgv-stat` | p element indicating total event count for a given day |
| `lgv-weekday` | p element for individual column label |


## Actively Develop

```bash
# clone repository
git clone <repo_url>

# update directory context
cd calendar

# run docker container
docker run \
  --rm \
  -it  \
  -v $(pwd):/project \
  -w /project \
  -p 8080:8080 \
  node \
  bash

# FROM INSIDE RUNNING CONTAINER

# install module
npm install .

# run development server
npm run start

# edit src/index.js
# add const c = new Calendar(data);
# add c.render(document.body);
# replace `data` with whatever data you want to develop with

# view visualization in browser at http://localhost:8080
```
