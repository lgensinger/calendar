import path from "path";
import { dirname } from "path";
import { fileURLToPath } from "url";

import webpack from "webpack";

const __dirname = dirname(fileURLToPath(import.meta.url));

const commonConfig = {

    entry: {
        index: "./src/index.js"
    },

    plugins: [
        new webpack.DefinePlugin({
            "process.env": {
                "LGV_HEIGHT": JSON.stringify(process.env.LGV_HEIGHT),
                "LGV_WIDTH": JSON.stringify(process.env.LGV_WIDTH)
            }
        })
    ],

    output: {
        filename: "calendar.bundle.js",
        path: path.resolve(__dirname, "dist"),
        clean: true
    },

    module: {
        rules: [
            {
                test: /\.css$/i,
                use: ["css-loader"]
            }
        ]
    }

 };

 export { commonConfig };
 export default commonConfig;
